# Docker base image

This docker image is used by most projects to simplify deployment.

## Local development

1. Download all the necessary repos (including Config) then `mvn install` in the following order:
    - Messenger
    - Global
    - Game
    - MCBungee
    - Ender
2. Run `docker-compose -f docker-compose.dev.yml up` to start the Redis and MongoDB containers.
3. Import the `mineclick.settings.json` MongoDB collection found in the Config repo. You can
   use [MongoDB Compass](https://www.mongodb.com/products/compass) to do this.
4. Find your player's MongoDB document in the `mineclick.players` collection and set your rank to `DEV`. Once again use
   MongoDB Compass to do this.
5. Run the following commands in separate terminals:
    - In the `folders/ender` folder, run `./test.sh` to run Ender (port 8088 for web; 8089 for api)
    - In the `folders/bungee` folder, run `./test.sh` to run Bungee (port 25566)
    - In the `folders/game` folder, run `./test.sh <path to Config root dir>` to run the Game
6. You can now connect to the server using the IP `localhost` and the port `25566`.

If you have any issues, please write in the `#dev` channel on Discord.