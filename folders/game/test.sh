#!/usr/bin/env bash
. ../common/gitUtil.sh

while true; do
  (
    # make a temporary folder
    folder=../../temp/game_$(head /dev/urandom | tr -dc a-z0-9 | head -c 5)
    mkdir -p "$folder"
    cp -R . "$folder"/
    cd "$folder" || (echo "No temp folder" && exit)

    # Extract world/region.tar.xz
    tar -C ./world/ -xzvf ./world/region.tar.gz

    # Download configurations
    mkdir config
    cp -r ${1:-~/sauce/mc/config}/dimensions ./config/
    cp -r ${1:-~/sauce/mc/config}/mineshaft ./config/

    # Build Game
    if [ -n "$1" ]; then
      echo "Installing Game..."
      (cd "$1" && mvn install)
    fi

    # Copy the plugins from local maven folder
    cp ~/.m2/repository/net/mineclick/core/Global/master/Global-master.jar ./plugins/Global.jar
    cp ~/.m2/repository/net/mineclick/game/Game/master/Game-master.jar ./plugins/Game.jar

    # run
    export QUICK_LOAD=true
    java -Xms2G -Xmx4G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -jar paper.jar --noconsole nogui

    # cleanup
    rm -rf "$folder"

    # sleep for 3 seconds
    echo "Restarting in 3 seconds... Hit CTRL+C again to exit"
    sleep 3
  )
done
