#!/usr/bin/env bash
# repo branch:master
clone() {
  (git clone --single-branch --branch "${2:-master}" https://token:"$GITLAB_TOKEN"@gitlab.com/mineclick/"$1".git && echo "Downloaded $1") ||
    (echo "Error downloading from gitlab" && exit 1)
}
