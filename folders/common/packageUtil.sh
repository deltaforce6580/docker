#!/usr/bin/env bash
parent_path=$(
  cd "$(dirname "${BASH_SOURCE[0]}")"
  pwd -P
)

# group name version output
download() {
  (wget --header "DEPLOY-TOKEN: $GITLAB_TOKEN" \
    "https://gitlab.com/api/v4/groups/mineclick/-/packages/maven/$1/$2/$3/$2-$3.jar" \
    -O "$4" && echo "Downloaded $4") || (echo "Error downloading $2 from the gitlab repo" && return 1)
}
