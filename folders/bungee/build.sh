#!/usr/bin/env bash
. ../common/packageUtil.sh

# Download MCBungee and move to plugins
download net/mineclick/bungee MCBungee "$BRANCH" MCBungee.jar
mv MCBungee.jar plugins

java -Xms2G -Xmx4G -jar waterfall.jar --noconsole
