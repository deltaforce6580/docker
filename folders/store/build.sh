#!/usr/bin/env bash
. ../common/packageUtil.sh

# Download Store
download net/mineclick/web Store "$BRANCH" Store.jar

# Run
java -jar Store.jar
